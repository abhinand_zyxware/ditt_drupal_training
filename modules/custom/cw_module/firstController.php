<?php
/**
* @file
*
*/

namespace Drupal\cw_module\Controller;

use Drupal\Core\Controller\controllerBase;

class FirstController extends controllerbase {
	public function content() {
		return array(
		  '#type' => 'markup' ,
		  '#markup' => t('Hello world'),
		 );
	}
}	