<?php 

namespace Drupal\twangmodule\Controller;
use Drupal\Core\Controller\ControllerBase;

class FirstController extends ControllerBase
 {
	public function content() {
		return array(
		  '#type' => 'markup',
		  '#markup' => t('Hello world'),
		  );
	}
 }
 