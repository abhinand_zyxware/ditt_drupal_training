<?php
/**
 * @file
 * Contains \Drupal\mo_khan\Controller\FirstController.
 */
 
namespace Drupal\mo_khan\Controller;
 
use Drupal\Core\Controller\ControllerBase;
 
class FirstController extends ControllerBase {
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => t('Hello from Mohan'),
    );
  }
}
