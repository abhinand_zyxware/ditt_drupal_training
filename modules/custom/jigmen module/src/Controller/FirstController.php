<?php
/**
 * @file
 * Contains \Drupal\jigmen_module\Controller\FirstController.
 */
 
namespace Drupal\jigmen_module\Controller;
 
use Drupal\Core\Controller\ControllerBase;
 
class FirstController extends ControllerBase {
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => t('jigmen says Hello world'),
    );
  }
}