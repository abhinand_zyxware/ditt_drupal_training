<?php

namespace Drupal\myblock\Controller;

use Drupal\examples\Utility\DescriptionTemplateTrait;

/**
 * Controller routines for block example routes.
 */
class myblockController {
  use DescriptionTemplateTrait;

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'block_example';
  }

}
