<?php

namespace Drupal\myblock\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\myblock\myblockStorage;
/**
 * Provides a 'Example: empty block' block.
 *
 * @Block(
 *   id = "my block",
 *   admin_label = @Translation("my block")
 * )
 */
class myblock extends BlockBase {

  /**
   * {@inheritdoc}
   *
   * The return value of the build() method is a renderable array. Returning an
   * empty array will result in empty block contents. The front end will not
   * display empty blocks.
   */
  /*public function build() {
    return [
      '#markup' => $this->t('hello world!'),
      '#title' => $this->t('My block'),
    ];
  } */
  
  public function build() {
	 $content = [];

        

        $rows = [];
        $headers = [t('Name'), t('Email'), t('Description')];
		$i = 0;
        foreach ($entries = myblockStorage::select() as $entry) {
			
            // Sanitize each entry.
            $rows[] = array_map('Drupal\Component\Utility\SafeMarkup::checkPlain', (array) $entry);
            
            $content['message'][$i] = [
               '#markup' => $rows[$i]['description'] . '<br><br>' . '- ' . $rows[$i]['name'] . '<br><br>',
            ];
            $i++;
        }
      /*  $content['table'] = [
          '#type' => 'table',
          '#header' => $headers,
          '#rows' => $rows,
          '#empty' => t('No entries available.'),
        ];
        
        // Don't cache this page.
        $content['#cache']['max-age'] = 0;
*/
        return $content;
  }


}
