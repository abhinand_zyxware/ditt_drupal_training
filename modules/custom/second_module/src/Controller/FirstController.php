<?php
/**
 * @file
 * Contains \Drupal\second_module\Controller\FirstController.
 */
 
namespace Drupal\second_module\Controller;
 
use Drupal\Core\Controller\ControllerBase;
 
class FirstController extends ControllerBase {
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => t('Hello world'),
    );
  }
}
