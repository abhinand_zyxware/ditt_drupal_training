<?php

namespace Drupal\Kinzang_module\Controller;
use Drupal\Core\Controller\ControllerBase;

class Kinzang_module_controller extends  ControllerBase {
	public function content() {
		return array( 
		'#type' => 'markup',
		'#markup'=> t('Hello Bhutan'),
		);
	}
}
